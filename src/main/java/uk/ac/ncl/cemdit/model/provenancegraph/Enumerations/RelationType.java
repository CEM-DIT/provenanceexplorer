package uk.ac.ncl.cemdit.model.provenancegraph.Enumerations;

public enum RelationType {
    USED,
    WASGENERATEDBY,
    WASDERIVEDFROM,
    WASATTRIBUTEDTO,
    WASASSOCIATEDWITH,
    WASINFORMEDBY,
    ACTEDONBEHALFOF,
    HADMEMBER,
    PARTGENERATEDBY
}
