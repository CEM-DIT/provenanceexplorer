package uk.ac.ncl.cemdit.view.integration;

import org.apache.log4j.Logger;
import uk.ac.ncl.cemdit.model.integration.IntegrationModel;
import uk.ac.ncl.cemdit.view.GraphStreamPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;

public class ResultsPanel extends JPanel implements ActionListener {
    private JButton viewProvenance = new JButton("View Provenance");
    private JButton viewData = new JButton("View Data");
    private JButton viewMatches = new JButton("View Matches");
    private JPanel viewButtonsHorizontalPanel = new JPanel();
    private JPanel repairedQueryPanel = new JPanel();
    private JTextArea repairedQuery = new JTextArea(4, 80);
    private MatchPanel matchPanel = new MatchPanel();
    private ProvenancePanel provenancePanel = new ProvenancePanel();
    private GraphStreamPanel graphStreamPanel = new GraphStreamPanel(null);
    private DataPanel dataPanel = new DataPanel();
    private Logger logger = Logger.getLogger(this.getClass());
    private ActionListener actionListener = this;
    private JScrollPane sp_matchPanel;

    public ResultsPanel(IntegrationModel integrationModel, Object eventsListener) {
        super();
        actionListener = (ActionListener) eventsListener;
        setLayout(new BorderLayout());

        viewProvenance.addActionListener(actionListener);
        viewData.addActionListener(actionListener);
        viewMatches.addActionListener(actionListener);

        viewMatches.setEnabled(false);

        viewButtonsHorizontalPanel.setLayout(new FlowLayout());
        viewButtonsHorizontalPanel.add(viewProvenance);
        viewButtonsHorizontalPanel.add(viewData);
        viewButtonsHorizontalPanel.add(viewMatches);
        viewButtonsHorizontalPanel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createTitledBorder("Views"),
                                BorderFactory.createEmptyBorder(5, 5, 5, 5)),
                        viewButtonsHorizontalPanel.getBorder()));


        repairedQuery.setText(integrationModel.getOriginalQuery());
        repairedQueryPanel.add(repairedQuery);
        repairedQuery.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createTitledBorder("Repaired Query"),
                                BorderFactory.createEmptyBorder(5, 5, 5, 5)),
                        repairedQuery.getBorder()));


        matchPanel = (MatchPanel) matchPanel.populateMatchPanel(matchPanel, integrationModel.getQueryResults(), actionListener);
        matchPanel.setSimilarity(integrationModel.getSimilarityScore());
        sp_matchPanel = new JScrollPane(matchPanel);
        sp_matchPanel.setPreferredSize(new Dimension(900, 250));
        logger.debug("Length of query results: " + integrationModel.getQueryResults().size());

        add(viewButtonsHorizontalPanel, BorderLayout.NORTH);
        add(repairedQueryPanel, BorderLayout.CENTER);
        add(sp_matchPanel, BorderLayout.SOUTH);

    }

    public void setButtons(boolean prov, boolean data, boolean matches) {
        viewProvenance.setEnabled(prov);
        viewMatches.setEnabled(matches);
        viewData.setEnabled(data);
        if (!prov) {
            graphStreamPanel.getViewPanel();
            sp_matchPanel.setViewportView(provenancePanel);
        }
        if (!matches) {
            sp_matchPanel.setViewportView(matchPanel);
        }
        if (!data) {
            sp_matchPanel.setViewportView(dataPanel);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
