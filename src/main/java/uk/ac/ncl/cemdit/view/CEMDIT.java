package uk.ac.ncl.cemdit.view;

import org.apache.log4j.Logger;
import uk.ac.ncl.cemdit.model.integration.IntegrationModel;
import uk.ac.ncl.cemdit.view.integration.CEMDITMainPanel;
import uk.ac.ncl.cemdit.model.integration.QueryResults;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CEMDIT extends JFrame  implements ActionListener {
    private CEMDITMainPanel mainPanel ;
    private IntegrationModel integrationModel = new IntegrationModel();
    private Logger logger = Logger.getLogger(this.getClass());


    CEMDIT() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image icon = toolkit.getImage(ClassLoader.getSystemResource("Logo.png"));
        setIconImage(icon);

        // read data for other results
        ArrayList<String> data = new ArrayList<>();
        data.add("1. sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)");
        data.add("2. sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)");
        data.add("3. sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)");
        data.add("4. sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)");
        data.add("5. sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)");

        integrationModel.setOtherResponses(data);
        integrationModel.setOriginalQuery("sensor(type(traffic), id, time(154630), measure, location(latitude,longitude), units)");
        String[] sourceLabel = {"Sensor", "traffic", "type", "id", "location", "latitude", "longitute", "measure", "time", "units", "units", "units", "units", "units"};
        String[] operators = {"=", ">", "=", "=", "X", ">", ">", ">", "=", "=", "=", "=", "=", "="};
        String[] targetLabel = {"Sensor", "Vehicles", "theme", "sensor_names", "", "sensor_centroid_latitude", "sensor_centroid_longitude", "count", "timestamp", "untis", "untis", "untis", "untis", "untis"};
        integrationModel.setSimilarityScore(0.7);
        ArrayList<QueryResults> queryResults = new ArrayList<>();
        for (int i = 0; i < sourceLabel.length; i++) {
            QueryResults queryResult = new QueryResults(sourceLabel[i], operators[i], targetLabel[i]);
            queryResults.add(queryResult);
        }
        integrationModel.setQueryResults(queryResults);
        integrationModel.setProvNFilename("data/PROV/PROV-DM.svg");

        mainPanel = new CEMDITMainPanel(integrationModel);

        getContentPane().add(mainPanel);
        pack();
        setVisible(true);
        setSize(1024, 768);
    }

    static public void main(String[] args) {
        CEMDIT mockup = new CEMDIT();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        logger.debug(e.getActionCommand());
     }
}

