package uk.ac.ncl.cemdit.view.integration;

import org.apache.log4j.Logger;
import uk.ac.ncl.cemdit.model.integration.IntegrationModel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;

public class CEMDITMainPanel extends JPanel implements ActionListener, ListSelectionListener {
    private JPanel queryPanel = new JPanel();
    private JTextArea queryTextArea = new JTextArea();
    private ResultsPanel resultPanel;
    private ResponsePanel otherPanel;
    private IntegrationModel integrationModel = new IntegrationModel();
    private Logger logger = Logger.getLogger(this.getClass());

    public CEMDITMainPanel(IntegrationModel integrationModel) {
        super();
        this.integrationModel = integrationModel;
        setup();
    }

    public CEMDITMainPanel() {
        super();
        setup();
    }

    private void setup() {
        queryPanel.setLayout(new BorderLayout());
        queryTextArea.setText(integrationModel.getOriginalQuery());
        resultPanel = new ResultsPanel(integrationModel, this);
        otherPanel = new ResponsePanel(integrationModel.getOtherResponses(), this);

        setLayout(new BorderLayout());
        setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createTitledBorder("CEM-DIT Data Visualisation Screen"),
                                BorderFactory.createEmptyBorder(5, 5, 5, 5)),
                        this.getBorder()));


        queryPanel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createTitledBorder("Your Query"),
                                BorderFactory.createEmptyBorder(5, 5, 5, 5)),
                        queryPanel.getBorder()));
        resultPanel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createTitledBorder("Top Ranked Response"),
                                BorderFactory.createEmptyBorder(5, 5, 5, 5)),
                        resultPanel.getBorder()));
        otherPanel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createTitledBorder("Other Responses"),
                                BorderFactory.createEmptyBorder(5, 5, 5, 5)),
                        otherPanel.getBorder()));

        queryPanel.add(queryTextArea);
        add(queryPanel, BorderLayout.NORTH);
        add(resultPanel, BorderLayout.CENTER);
        add(otherPanel, BorderLayout.PAGE_END);
    }

    public IntegrationModel getIntegrationModel() {
        return integrationModel;
    }

    public void setIntegrationModel(IntegrationModel integrationModel) {
        this.integrationModel = integrationModel;
    }

    public void setProvenancePanel() {
        resultPanel.setButtons(false, true, true);
    }

    public void setDataPanel() {
        resultPanel.setButtons(true, false, true);

    }

    public void setMatchPanel() {
        resultPanel.setButtons(true, true, false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.debug(e.getActionCommand());
        switch (e.getActionCommand()) {
            case "View Provenance":
                if (e.getActionCommand().equals("View Provenance")) {
                    logger.debug(e.getActionCommand());
                    IntegrationModel integrationModel = new IntegrationModel();
                    File svgFile = new File(integrationModel.getProvNFilename());
                    try {
                        logger.debug("Filename: " + svgFile.toURI().toURL().toString());
                        integrationModel.getProvenancePanel().getSVGCanvas().setURI(svgFile.toURI().toURL().toString());
                    } catch (MalformedURLException e1) {
                        e1.printStackTrace();
                    }
                    setProvenancePanel();
                }
                break;
            case "View Data":
                setDataPanel();
                break;
            case "View Matches":
                setMatchPanel();
                break;
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        JList jList = (JList) e.getSource();
        logger.debug(e.getFirstIndex());
    }
}
