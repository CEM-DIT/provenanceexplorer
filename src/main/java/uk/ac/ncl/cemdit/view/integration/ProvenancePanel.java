package uk.ac.ncl.cemdit.view.integration;

import uk.ac.ncl.cemdit.view.SVGCanvas;

import javax.swing.*;
import java.awt.*;

public class ProvenancePanel extends JPanel {
    private JScrollPane sp_prov = new JScrollPane();
    private SVGCanvas svgCanvas = new SVGCanvas();

    public ProvenancePanel() {
        setLayout(new FlowLayout());
        sp_prov.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        sp_prov.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp_prov.add(svgCanvas);
        add("Prov ProvGraph (svgCanvas)", svgCanvas);


    }

    public SVGCanvas getSVGCanvas() {
        return svgCanvas;
    }

    public void setSVGCanvas(SVGCanvas prov) {
        this.svgCanvas = prov;
    }
}
